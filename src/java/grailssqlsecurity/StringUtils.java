package grailssqlsecurity;
/**
 * Created by sduwal on 4/15/16.
 */
public class StringUtils {
    public static String clean(String content){
        return content.replaceAll("[^a-zA-Z0-9 ]", "");
    }
}
