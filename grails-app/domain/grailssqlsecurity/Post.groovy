package grailssqlsecurity

class Post {

    String id
    String title
    String content
    String author
    int views =0

    static constraints = {
        title blank: false
        content blank: false, size: 10..100
        author blank: false
    }
}
