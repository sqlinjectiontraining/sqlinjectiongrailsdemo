<%--
  Created by IntelliJ IDEA.
  User: trustgeek
  Date: 4/14/2016
  Time: 9:21 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Index - GrailsSecurity</title>
    <meta name="layout" content="main"/>
</head>

<body>
<g:if test="${session?.user}">
    <h1>Grails SQL Injection DEMO - Posts</h1>
    <g:form controller="posts" action="search">
        <div class="search">
            Search Contacts|
            <input type="text" name="searchString" value="${params.searchString}" />|
            <g:submitButton class="button" name="submitButton" value="Search" />
        </div>
    </g:form>
    <g:each in="${postList}" var="posts">
        <b>${posts.id}</b> | ${posts.author} | ${posts.title} | ${posts.content}<br>
    </g:each>

</g:if>
<g:else>

</g:else>

</body>
</html>