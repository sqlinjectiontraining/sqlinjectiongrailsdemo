<%--
  Created by IntelliJ IDEA.
  User: sduwal
  Date: 4/19/16
  Time: 4:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Grails SQL Security - Add Post</title>
    <meta name="layout" content="main"/>
</head>

<body>
<g:if test="${session?.user}">
    <g:form class="inputform" controller="posts" action="add">
        <fieldset>
            <legend>User Registration</legend>

            <p class="info">
                Add title and Content
            </p>
            <g:hasErrors bean="${user}">
                <div class="errors">
                    <g:renderErrors bean="${user}"/>
                </div>
            </g:hasErrors>
            <p>
                <label for="title">Title</label>
                <g:textField name="title"
                             class="${hasErrors(bean: user, field: 'title', 'errors')}"/>
            </p>

            <p>
                <label for="content">Content</label>
                <g:textArea name="content" rows="5" cols="50"
                            class="${hasErrors(bean: user, field: 'content', 'errors')}"/>
            </p>
            %{--<g:hiddenField name="author" value="${session.user}"--}%
                           %{--class="${hasErrors(bean: user, field: 'author', 'errors')}"/>--}%

            <p class="button">
                <label>&nbsp;</label>
                <g:submitButton class="button" name="submitButton" value="Add Post"/>
            </p>
        </fieldset>
    </g:form>
</g:if>
<g:else>

</g:else>
</body>
</html>