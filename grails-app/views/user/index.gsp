<%--
  Created by IntelliJ IDEA.
  User: trustgeek
  Date: 4/14/2016
  Time: 9:21 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Index - GrailsSecurity</title>
    <meta name="layout" content="main"/>
</head>

<body>
<g:if test="${session?.user}">
    <h1>Grails SQL Injection DEMO</h1>
    <img src="${resource(dir: 'images', file: '2.png')}" alt="Successfully Logged in"/>
    <p><a class="arrow-button" name="Posts" href="${resource(dir:'posts', file:'index.gsp')}">Posts</a></p>
</g:if>
<g:else>
    <h1>Grails SQL Injection DEMO</h1>
    <br>
    This is index page of the application for Grails SQL Injection demo.<br>
    This application demonstrates three different types of programming<br>
    mistakes that normally programmers make. <br>
    <br>
    <g:each var="list"
            in="${["Direct SQL query execution methods", "HQL execution method", "Criteria creation in Grails"]}"
            status="counter">
        ${counter + 1}. <i><b>${list}</b></i> <br/>
    </g:each>

        <p><a class="btn-link" name="login" href="login.gsp">Login</a></p>

        <p><a class="btn-link" name="register" href="register.gsp">Register</a></p>

</g:else>

</body>
</html>