<%--
  Created by IntelliJ IDEA.
  User: trustgeek
  Date: 4/11/2016
  Time: 9:23 PM
--%>
<html>
<head>
    <title>GrailsSecurity - Homepage</title>
    <meta name="layout" content="main" />
</head>
<body>
<h1>Login</h1>
<g:if test="${session?.user}">

</g:if>
<g:else>
    <g:form class="inputform" style="width:50%;" controller="user" action="login" method="POST">
        <fieldset>
            <legend>Login</legend>
            <p class="info">
                Please login with your username and password. <br />
            </p>
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <p>
                <label for="username">Username</label>
                <g:textField name="username" />
            </p>
            <p>
                <label for="password">Password</label>
                <g:passwordField name="password" />
            </p>
            <p class="button">
                <label>&nbsp;</label>
                <g:submitButton class="button" name="submitButton" value="Login" />
                <a class="btn-link" name="register" href="${resource(dir:'user', file:'register.gsp')}">Register</a>
            </p>
        </fieldset>
    </g:form>

</g:else>
</body>
</html>