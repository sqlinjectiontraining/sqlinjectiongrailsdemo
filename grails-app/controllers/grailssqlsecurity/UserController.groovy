package grailssqlsecurity

import grails.converters.JSON
import org.hibernate.SessionFactory
import org.hibernate.Transaction

class UserController {

    def index = {
    }
    def register = {
        if (request.method == 'POST') {
            def u = new User(params)
            u.password_hashed = params.password.encodeAsBase64()
//            u.password_hashed = params.password

            if (!u.save()) {
                return [user: u]
            } else {
                session.user = u
                redirect(controller: 'user')
            }
        } else if (session.user) {
            redirect(controller: 'user')
        }
    }

    def login = {
        if (request.method == 'POST') {
            println(params)

            // General SQL method
            // unencrypted password
//            def passwordHashed = params.password
//            def query = "from User u where u.username='" + params.username + "' and u.password_hashed='" + passwordHashed + "'";

            // encrypted password
            def passwordHashed = params.password.encodeAsBase64()
            def query = "from User u where u.username='" + params.username + "' and u.password_hashed='" + passwordHashed + "'";

            // possible solution - password shouldn't be null
//                               - password should be encoded
//                               - remove invalid characters
//            def passwordHashed = params.password.encodeAsBase64()
//            def username = params.username //StringUtils.clean(params.username)
//            def query = "from User u where u.username='" + username + "' and u.password_hashed='" + passwordHashed + "'";
//
            println(query)
            def userList = User.executeQuery(query)
            println "userList = "+userList


            // HQL Query Method
//            def passwordHashed = params.password.encodeAsBase64()
//            def userCriteria = User.createCriteria()

//            def userList = userCriteria.list {
//                and {
//                    eq("username", params.username)
//                    eq("password_hashed", passwordHashed)
//                }
//            }

//            def passwordHashed = params.password.encodeAsBase64()
//            String query = "from User u where u.username='${params.username}' and u.password_hashed='${passwordHashed}'"
//            println query
//            def userList = User.executeQuery(query)

            // solution for HQL Query - parameterized query
//            def userList = User.executeQuery("from User u where u.username=:username and u.password_hashed=:password", [username: params.username, password: passwordHashed])
//            def userList = User.findByUsernameAndPassword_hashed(params.username, passwordHashed)

            if (userList) {
                // username and password match -> log in
                session.user = userList
                redirect(controller: 'user')
            } else {
                flash.message = "User not found"
                redirect(controller: 'user', action: 'login')
            }
        } else if (session.getAttribute("user")) {
            println(request.method)
            redirect(controller: 'user', action: 'index')
        }
    }
    def logout = {
        session.invalidate()
        redirect(controller: 'user')
        render "Logged out"
    }
}
