package grailssqlsecurity

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

class PostsController {

    def index() {
        def username = session.user
        if (username) {
            List<Post> postList = Post.findAll()
            if (postList) {
                [postList: postList]
            } else {
                redirect(controller: "posts")
            }
        } else {
            redirect(controller: "user", action: "index")
        }
    }

    def add() {

        def username = session.user
        if (username) {
            println params
            println request.method
            if (request.method == 'POST') {
                params.put("author", username.username.join(""))
                println "username = $username.username"
                println(params)
                if (savePost(params)) {
                    redirect(controller: "posts", action: "index")
                } else {
                    render "Error Saving"
                }
            }

        } else {
            redirect(controller: "user", action: "login")
        }
    }

    private boolean savePost(GrailsParameterMap params) {
        try {
            new Post(title: params.title,
                    content: params.content,
                    author: params.author
            ).save(flush: true)
            return true
        } catch (Exception e) {
            e.printStackTrace()
            return false
        }

    }

    def search() {
        def searchString = params.searchString
        println "SearchString : " + searchString
        if (searchString) {
            def query = "from Post p where p.id=" + searchString
            println "query = $query"
            def srchResults = Post.executeQuery(query)
            println "srchResults = $srchResults"
            render(view: "search",
                    model: [postList: srchResults])
        } else {
            redirect(action: "posts")
        }
    }
}
