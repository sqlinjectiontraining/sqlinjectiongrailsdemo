class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/user/login")
        "/login"(view:"/user/login")
        "/register"(view:"/user/register")
        "/posts"(view:"/posts/index")
//        "/"(controller: "login", action: "index")
        "500"(view:'/error')
	}
}
